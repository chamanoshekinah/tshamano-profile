import logo from "./logo.svg";
import "./App.css";
import React from "react";
import About from "./components/About";
import Skills from "./components/Skills";
import HireMe from "./components/HireMe";
import Contact from "./components/Contact";
import HeaderComponent from "./components/HeaderComponent";

function App() {
  return (
    <React.Fragment>
      <div class="fh5co-loader"></div>

      <div id="page">
       <HeaderComponent />
       <About />
       <Skills />
       <HireMe />
       <Contact />
      </div>
    </React.Fragment>
  );
}

export default App;
