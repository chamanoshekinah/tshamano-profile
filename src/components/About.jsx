import React from 'react'

const About = (props)=>{

    return(
        <React.Fragment>
             <div id="fh5co-about" class="animate-box">
          <div class="container">
            <div class="row">
              <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                <h2>About Me</h2>
              </div>
            </div>
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-11">
                <h2>Hello There!</h2>
                <p>
                  {" "}
                  I am Mobane Tshamano, I studied Information Technology and
                  Computer Science at Vhembe TVET college, where I learned
                  programmimg, System Analysis and Database. I also did a
                  certificate in Big data Technology and Application where I
                  have used Python for my data collecting, analysing and finding
                  treads in the data set. I have skills in C#, NET CORE,
                  HTML/CSS, SQL, Javascript, MVC and bootstrap.
                </p>
                <p>
                  {" "}
                  I also did a one year internship as a web developer, designing
                  and implementing different websites for companies using PHP,
                  HTML,WordPress, CSS and Javascript. Creating attractive and
                  userfriendy websites according to clients specifications,
                  maintaining and upgrading existing system, Troubleshooting and
                  fixing bugs on the system and gathering user requirments and
                  specifications.
                </p>
                <p>
                  <p>
                    {" "}
                    I enjoy being challenged and engaging with projects that
                    require me to work outside my comfort and knowledge set, as
                    continuing to learn new languages and development techniques
                    are important to me and the success of your organization.
                  </p>
                </p>
              </div>
            </div>
          </div>
        </div>
        </React.Fragment>
    )
}

export default About