import React from 'react'

const Skills = (props)=>{

    return(
        <React.Fragment>
             <div id="fh5co-skills" class="animate-box">
          <div class="container">
            <div class="row">
              <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                <h2>Skills</h2>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="progress-wrap">
                  <h3>
                    <span class="name-left">HTML/CSS</span>
                    <span class="value-right">95%</span>
                  </h3>
                  <div class="progress">
                    <div
                      class="progress-bar progress-bar-1 progress-bar-striped active"
                      role="progressbar"
                      aria-valuenow="90"
                      aria-valuemin="0"
                      aria-valuemax="100"
                      style={{ width: "90%" }}
                    ></div>
                  </div>
                </div>
                <div class="progress-wrap">
                  <h3>
                    <span class="name-left">C#</span>
                    <span class="value-right">85%</span>
                  </h3>
                  <div class="progress">
                    <div
                      class="progress-bar progress-bar-4 progress-bar-striped active"
                      role="progressbar"
                      aria-valuenow="85"
                      aria-valuemin="0"
                      aria-valuemax="100"
                      style={{ width: "85%" }}
                    ></div>
                  </div>
                </div>
                <div class="progress-wrap">
                  <h3>
                    <span class="name-left">React</span>
                    <span class="value-right">20%</span>
                  </h3>
                  <div class="progress">
                    <div
                      class="progress-bar progress-bar-2 progress-bar-striped active"
                      role="progressbar"
                      aria-valuenow="90"
                      aria-valuemin="0"
                      aria-valuemax="100"
                      style={{ width: "90%" }}
                    ></div>
                  </div>
                </div>
                <div class="progress-wrap">
                  <h3>
                    <span class="name-left">PHP</span>
                    <span class="value-right">80%</span>
                  </h3>
                  <div class="progress">
                    <div
                      class="progress-bar progress-bar-3 progress-bar-striped active"
                      role="progressbar"
                      aria-valuenow="80"
                      aria-valuemin="0"
                      aria-valuemax="100"
                      style={{ width: "80%" }}
                    ></div>
                  </div>
                </div>
                <div class="progress-wrap">
                  <h3>
                    <span class="name-left">Razor</span>
                    <span class="value-right">85%</span>
                  </h3>
                  <div class="progress">
                    <div
                      class="progress-bar progress-bar-4 progress-bar-striped active"
                      role="progressbar"
                      aria-valuenow="85"
                      aria-valuemin="0"
                      aria-valuemax="100"
                      style={{ width: "85%" }}
                    ></div>
                  </div>
                </div>
                <div class="progress-wrap">
                  <h3>
                    <span class="name-left">SQL</span>
                    <span class="value-right">70%</span>
                  </h3>
                  <div class="progress">
                    <div
                      class="progress-bar progress-bar-5 progress-bar-striped active"
                      role="progressbar"
                      aria-valuenow="100"
                      aria-valuemin="0"
                      aria-valuemax="100"
                      style={{ width: "100%" }}
                    ></div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="progress-wrap">
                  <h3>
                    <span class="name-left">GIT</span>
                    <span class="value-right">100%</span>
                  </h3>
                  <div class="progress">
                    <div
                      class="progress-bar progress-bar-5 progress-bar-striped active"
                      role="progressbar"
                      aria-valuenow="100"
                      aria-valuemin="0"
                      aria-valuemax="100"
                      style={{ width: "100%" }}
                    ></div>
                  </div>
                </div>
                <div class="progress-wrap">
                  <h3>
                    <span class="name-left">Web API</span>
                    <span class="value-right">70%</span>
                  </h3>
                  <div class="progress">
                    <div
                      class="progress-bar progress-bar-striped active"
                      role="progressbar"
                      aria-valuenow="70"
                      aria-valuemin="0"
                      aria-valuemax="100"
                      style={{ width: "70%" }}
                    ></div>
                  </div>
                </div>
                <div class="progress-wrap">
                  <h3>
                    <span class="name-left">jQuery</span>
                    <span class="value-right">75%</span>
                  </h3>
                  <div class="progress">
                    <div
                      class="progress-bar progress-bar-3 progress-bar-striped active"
                      role="progressbar"
                      aria-valuenow="75"
                      aria-valuemin="0"
                      aria-valuemax="100"
                      style={{ width: "75%" }}
                    ></div>
                  </div>
                </div>
                <div class="progress-wrap">
                  <h3>
                    <span class="name-left">AzureDevOps</span>
                    <span class="value-right">75%</span>
                  </h3>
                  <div class="progress">
                    <div
                      class="progress-bar progress-bar-3 progress-bar-striped active"
                      role="progressbar"
                      aria-valuenow="75"
                      aria-valuemin="0"
                      aria-valuemax="100"
                      style={{ width: "75%" }}
                    ></div>
                  </div>
                </div>
                <div class="progress-wrap">
                  <h3>
                    <span class="name-left">Python</span>
                    <span class="value-right">85%</span>
                  </h3>
                  <div class="progress">
                    <div
                      class="progress-bar progress-bar-1 progress-bar-striped active"
                      role="progressbar"
                      aria-valuenow="85"
                      aria-valuemin="0"
                      aria-valuemax="100"
                      style={{ width: "85%" }}
                    ></div>
                  </div>
                </div>
                <div class="progress-wrap">
                  <h3>
                    <span class="name-left">Javascript</span>
                    <span class="value-right">75%</span>
                  </h3>
                  <div class="progress">
                    <div
                      class="progress-bar progress-bar-3 progress-bar-striped active"
                      role="progressbar"
                      aria-valuenow="75"
                      aria-valuemin="0"
                      aria-valuemax="100"
                      style={{ width: "75%" }}
                    ></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </React.Fragment>
    )
}

export default Skills