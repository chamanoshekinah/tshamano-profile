import React from "react";

const HireMe = (props) => {
  return (
    <React.Fragment>
      <div id="fh5co-started" class="fh5co-bg-dark">
        <div class="overlay"></div>
        <div class="container">
          <div class="row animate-box">
            <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
              <h2>Hire Me!</h2>
              <p>
                I want to be a developer at SovTech because I believe am the
                best candicate for this program. I have a good knowledge and
                foundation for programming. I enjoy learning new things, finding
                new ideas and new solutions. My passion is solving everyday
                problems and meeting new challenges. I believe this program
                would help me grow my real-world experience in software
                development industry and help me futher develop and refine my
                application design skills. I believe that my skills and passion
                match the company’s drive and capabilities
              </p>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default HireMe;
