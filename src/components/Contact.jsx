import React from "react";

const Contact = (props) => {
  return (
    <React.Fragment>
      <div id="fh5co-consult">
        <div
          class="video fh5co-video"
          style={{ backgroundImage: "url(images/p.jpg)" }}
        >
          <div class="overlay"></div>
        </div>
        {/* </div> */}
        <div class="choose animate-box" style={{ backgroundColor: "#f5f3f3" }}>
          <div class="col-md-12">
            <h2>Contact</h2>

            <div class="contact-page">
              <div class="contact-info">
                <div class="item">
                  <i class="icon fas fa-home"></i>&nbsp;&nbsp;
                  3315 Saffron Street Diepsloot 2189
                </div>
                <div class="item">
                  <i class="icon fas fa-phone"></i>&nbsp;&nbsp;
                  +2779 4250 319
                </div>
                <div class="item">
                  <i class="icon fas fa-envelope"></i>&nbsp;&nbsp;
                  mobane.ts@gmail.com
                </div>
              </div>

              <form action="#">
                <h2>Get in touch</h2>
                <div class="row form-group">
                  <div class="col-md-6">
                    <input
                      type="text"
                      id="fname"
                      class="form-control"
                      placeholder="Your firstname"
                    />
                  </div>
                </div>
                <div class="row form-group">
                  <div class="col-md-6">
                    <input
                      type="text"
                      id="lname"
                      class="form-control"
                      placeholder="Your lastname"
                    />
                  </div>
                </div>

                <div class="row form-group">
                  <div class="col-md-12">
                    <input
                      type="text"
                      id="email"
                      class="form-control"
                      placeholder="Your email address"
                    />
                  </div>
                </div>

                <div class="row form-group">
                  <div class="col-md-12">
                    <input
                      type="text"
                      id="subject"
                      class="form-control"
                      placeholder="Your subject of this message"
                    />
                  </div>
                </div>

                <div class="row form-group">
                  <div class="col-md-12">
                    <textarea
                      name="message"
                      id="message"
                      cols="30"
                      rows="10"
                      class="form-control"
                      placeholder="Say something about us"
                    ></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <input
                    type="submit"
                    value="Send Message"
                    class="btn btn-primary"
                  />
                </div>
              </form>
              {/* </div> */}
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Contact;
