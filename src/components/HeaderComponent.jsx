import React from 'react'

const HeaderComponent = (props)=>{

    return(
        <React.Fragment>
             <header
          id="fh5co-header"
          class="fh5co-cover js-fullheight"
          role="banner"
          style={{ backgroundImage: "url(images/cover_bg_3.jpg)" }}
          data-stellar-background-ratio="0.5"
        >
          <div class="overlay"></div>
          <div class="container">
            <div class="row">
              <div class="col-md-8 col-md-offset-2 text-center">
                <div class="display-t js-fullheight">
                  <div
                    class="display-tc js-fullheight animate-box"
                    data-animate-effect="fadeIn"
                  >
                    <div
                      class="profile-thumb"
                      style={{ background: "url(images/ppp.jpg)" }}
                    ></div>
                    <h1>
                      <span>Mobane Tshamano</span>
                    </h1>
                    <h3>
                      <span> Software Developer </span>
                    </h3>
                    <p>
                      <ul class="fh5co-social-icons">
                        <li>
                          <a
                            target="_blank"
                            href="https://twitter.com/sharmywachamy?t=bVvBfrAATaoCE4Dtwf18Yg&s=09"
                          >
                            <i class="icon-twitter2"></i>
                          </a>
                        </li>
                        <li>
                          <a
                            target="_blank"
                            href="https://www.facebook.com/chamano.mobane"
                          >
                            <i class="icon-facebook2"></i>
                          </a>
                        </li>
                        <li>
                          <a
                            target="_blank"
                            href="https://www.linkedin.com/in/tshamano-mobane-a65645176"
                          >
                            <i class="icon-linkedin2"></i>
                          </a>
                        </li>
                        <li>
                          <a
                            target="_blank"
                            href="https://github.com/Tshamano-1995"
                          >
                            <i class="icon-github"></i>
                          </a>
                        </li>
                      </ul>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>
        </React.Fragment>
    )
}

export default HeaderComponent